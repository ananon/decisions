# Design Kustomize Implementation

* Status: proposed 
* Deciders: 
* Date: 10-02-2022


## Context and Problem Statement

Our goal is to be able to manage our clusters configurations using GitOps approach with Kustomize via ArgoCD.

## Decision Drivers

* Centralization: We want all the configurations to be placed in a central location, so we are able to manage our clusters configurations from a centralized configurations hub (gitlab), which will be the single source of truth for configurations.  

* Granularity: We want to be able to deploy cluster-specific configurations in order to create wide variety of tailor made solutions for our customers.  

* As-a-service methodology: The configurations deployment should be automated. Configurations deployment should be called through RESTful API, this way we can integrate it with automated processes easily (e.g. cluster deployment workflow).  

* Continuous Deployment: We might also deploy configurations occasionaly after cluster installation, so this process should also be automated and easy to do.  

* Scalability: We should keep in mind that our managed clusters number is expected to grow fast.  

* Modularity: More configurations and patches will be added with time and as the amount of clients increases.

## Considered Options

* Using explicit approach in which we create something like an app-of-configurations for every managed cluster. By doing that we are able to customize every cluster with different configs and patches, decide which configs to apply for every different cluster, and create seperate overlays per cluster easily.  

* Using implicit approach in which we use ApplicationSets to apply a set configs and patches for a set of clusters (combination of ApplicationSet and ServiceSet). By doing that we are able to apply configurations for a bulk of clusters at once.  

* A hybrid approach, combining the best of both approaches with some trade offs.

## Pros and Cons of the Options

### Explicit approach

* Granular GitOps management. Create tailor-made configuration for each cluster seperately.  

* Issues are isolated between different clusters. Each cluster's issues are solved specifically for the same cluster, without affecting any other cluster configurations.  

* Management overhead at large scale. Respository/directory/file for each cluster will be difficult to manage at large scale.

* Requires many automated processes. In order to provide configurations-as-a-service we should automate the process of adding new managed cluster or new configurations with GitLab CI (or any other similar tool). These "CI" processes might be complicated to implement and maintain, and take time to complete.  

* Hard to track small changes, as the changes are individual per cluster.  

### Implicit approach

* Bulk management. Configure groups of clusters at once. There is no specific configuration per cluster, simple and easy to implement and maintain.  

* Simple to scale. Clusters can be added or removed easily from the groups/bulks of managed clusters.  

* No granular management. All clusters (in a certain group) have the same configurations. The clusters configurations are managed in groups. Can't cutomize every cluster differently.  

* Can't address cluster-specific problems using GitOps (because the clusters are managed as cattle)  

## Decision Outcome

We chose to implement a hybrid approach.  
To achieve this we created an app of configurations, which is an argo application deploying another argo apps, and each app deploys kustomize repository.  

1. The app of configurations is a kustomize application, which is deployed by an applicationSet (cloudletSet). The app of configurations and the configurations themselves (e.g - ca-bundle config, custom-kubelet config, etc) reside in different gitlab groups. This is to seperate between the kustomize applications of the configurations (the configurations' resources manifests) and the argo apps which deploy those configurations. Refer to the miro board in [Links](#links) section for a full architecture draw.  

2. Each configuration (e.g ca-bundle config, custom-kubelet config, etc) has it's own git repository (under configurations group). This makes it easier to create CI processes for each configuration seperately if necessary, and the repos are relatively small and lightweight and handled better by argocd.

3. The app of configurations for a group of clusters (multinode cloudlets for instance) repository contains a base/, contains argo applications which deploy the base version of each configuration for all clusters. The repository also contains an overlays/, which holds a kustomization.yaml file for each managed cluster. This enables us to deploy different configurations in different versions for each cluster that needs to drift from the base configuration for some reason.

4. Under the base/ we chose that each 'configuration' (it's just the argo application manifest deploying the configuration) will be in a dedicated directory, which contains a kustomization.yaml file, so kustomize will be able to address each configuration under the base/ seperatly. It means that it is possible to define explicitly which configurations to deploy in any overlay like this: `- ../../base/config-name` where config name is the directory of the specific configuration.


### Positive Consequences

* [e.g., improvement of quality attribute satisfaction, follow-up decisions required, …]
* …

### Negative Consequences

* [e.g., compromising quality attribute, follow-up decisions required, …]
* …


## Architecture Decisions - Adding a new cluster
### AD-1
To apply baseline configurations to a cluster, add cluster to a CloudletSet.

### AD-2
An application is created per cluster in a cloudletSet.

### AD-3
Each application created for a cluster represents an app of configurations.

### AD-4
An app of configurations is an argo application which deploys configurations per cluster.

### AD-5
The app of configurations resides in one git repository, and the cloudletSet resides in another git repository.

### AD-6
The app of configurations repository contains two directories: base/ and overlays/

### AD-7
Each cluster's app of configurations is represented as a directory in gitlab, e.g - overlays/takash111.

### AD-8
Each overlays/takashX contains kustomization.yaml file.

### AD-9
The kustomization.yaml points to ../../base directory.

### AD-10
For each argo application resource in base/, the kustomization.yaml replaces the destination server to be the overlayed cluster's api server.

### AD-11
The base/ contains kustomization.yaml file and a directory for each configuration, e.g - base/ingress-controller-patch.

### AD-12
The kustomization.yaml points to each directory in the base/.

### AD-13
Each directory under base/ contains a reference to a certain configuration. The configuration is represented by an argo application.

### AD-14
The argo application refers to a git repository which contains the configurations kubernetes resources, e.g - configurations/catalogsource-patch (updates catalogsource resource on the cluster)

### AD-15
The configurations reside in a gitlab group, where each configuration is a repository.

### AD-16
Every configuration repository consists of base/ and overlays/

### AD-17
Each configuration in the base app of configurations is deployed in it's base version.

## Architecture Decisions - Changing configuration in specific cluster
### AD-1
Create a directory under overlays/ in the configuration repository, e.g - catalogsource-patch/overlays/takash111.

### AD-2
The specific cluster overlay should be referenced in the app of configurations of this cluster.

### AD-3
To change the configuration's overlay, the kustomization.yaml in the cluster's app of configurations patches the argo application of the specific configuration.

### AD-4
The overlay path is patched using inline JSON6902 in this format:
```
- op: replace
  path: /spec/source/path
  value: overlays/OVERLAY
```

### AD-5
The patch replaces the argo application's path in the git repository of the configuration.

## Architecture Decisions - Change configuration in multiple clusters
### AD-1
To make changes in a certain configuration for a group of clusters, create a directory under overlays/ in the configuration repository, e.g - infra-nodes-patch/overlays/five-worker-nodes.

### AD-2
The overlay should be referenced in the app of configurations of each cluster in the group

## Links
* Miro board - Cloudlet ADR: https://miro.com/app/board/uXjVOUdf5uw=/?moveToWidget=3458764520432601803&cot=14
